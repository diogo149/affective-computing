from transform_data import transform_all, transform_data
from bhtsne import bh_tsne
from numpy import array
from filter_rows import filter_rows

default_perplexity = 30


def bhtsne_fit_transform(X):
    X_list = list(X)
    output = [result for result in bh_tsne(X_list, verbose=False, perplexity=default_perplexity)]
    return array(output)


transform_name = "bhtsnetst"
transform = bhtsne_fit_transform


def bhtsne_all():
    transform_all(bhtsne_fit_transform, transform_name + "_" + str(default_perplexity))


def bhtsne_one(folder_name, game, subject):
    transform_data(folder_name, game, subject, transform, transform_name)


def bhtsne_filter_fit_transform(X):
    Y = filter_rows(X, kept_features=["mean", "std", "median", "var", "dom_f", "dom2_f", "fmean"])
    return bhtsne_fit_transform(Y)


def bhtsne_all_filter():
    transform_all(bhtsne_filter_fit_transform, "bhtsne_filter_" + str(default_perplexity))

if __name__ == "__main__":
    if 0:
        bhtsne_all()
    if 1:
        bhtsne_one("2s", "escape", 15)
    if 0:
        bhtsne_all_filter()
