%% load data
load_data = 1;
if(load_data)
    trainX = csvread('csv/trainX.csv',1);
    N = size(trainX,1);
    D = size(trainX,2);
    trainY = csvread('csv/trainY.csv',1);
    validX = csvread('csv/validX.csv',1);
    V = size(validX,1);
end;
