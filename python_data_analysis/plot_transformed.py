
import matplotlib.pyplot as plt
from num_to_color import num_to_color


def plot_transformed(X, Y, filename=None):
    for row in range(X.shape[0]):
        plt.plot(X[row, 0], X[row, 1], 'o', color=num_to_color(Y[row]))
    if filename is not None:
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()
