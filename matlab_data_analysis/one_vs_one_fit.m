function [ pred ] = one_vs_one_fit( machine_fitter, trainX, trainY, testX )
    cats = unique(trainY);
    ovo_size = 1/2. * (-1+ length(cats) ) * length(cats);
    ovo_pred = zeros(size(testX,1),ovo_size);
    ovo_iter = 1;
    for i=1:length(cats)
        for j=(i+1):length(cats)
            %fprintf('OVO Pair %d\n',ovo_iter);
            pair_ind = (trainY==cats(i)) | (trainY==cats(j));
            pairTrainX = trainX(pair_ind,:);
            pairTrainY = trainY(pair_ind,:);
            ovo_pred(:,ovo_iter) = machine_fitter(pairTrainX,pairTrainY,testX);
            ovo_iter = ovo_iter+1;
        end;
    end;
    %pred = max(ovo_pred,[],2);
    pred = mode(ovo_pred,2);
end

