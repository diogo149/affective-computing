%% File Constants
reset_test_train = 0;
half_second_escape = [01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43];
half_second_sahara = [01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43];
two_second_escape = [01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43];
two_second_sahara = [01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43];
dataset_tuples = {0.5, 'escape', half_second_escape;
            0.5, 'sahara', half_second_sahara;
            2, 'escape', two_second_escape;
            2, 'sahara', two_second_sahara};

chosen_dataset = dataset_tuples(3,:);

emotion_columns = {'EMGmean', 'EMGstd', 'EMGmedian', 'EMGvar', ...
                   'EMGmean_d', 'EMGstd_d', 'EMGmedian_d', 'EMGvar_d', ...
                   'EMGmean_2d', 'EMGstd_2d', 'EMGmedian_2d', 'EMGvar_2d', ...
                   'EMGdom_f', 'EMGdom2_f', 'EMGfmean', ...
                   'SCmean', 'SCstd', 'SCmedian', 'SCvar', ...
                   'SCmean_d', 'SCstd_d', 'SCmedian_d', 'SCvar_d', ...
                   'SCmean_2d', 'SCstd_2d', 'SCmedian_2d', 'SCvar_2d', ...
                   'SCdom_f', 'SCdom2_f', 'SCfmean', ...
                   'BVPmean', 'BVPstd', 'BVPmedian', 'BVPvar', ...
                   'BVPmean_d', 'BVPstd_d', 'BVPmedian_d', 'BVPvar_d', ...
                   'BVPmean_2d', 'BVPstd_2d', 'BVPmedian_2d', 'BVPvar_2d', ...
                   'BVPdom_f', 'BVPdom2_f', 'BVPfmean', ...
                   'EMG2mean', 'EMG2std', 'EMG2median', 'EMG2var', ...
                   'EMG2mean_d', 'EMG2std_d', 'EMG2median_d', 'EMG2var_d', ...
                   'EMG2mean_2d', 'EMG2std_2d', 'EMG2median_2d', 'EMG2var_2d', ...
                   'EMG2dom_f', 'EMG2dom2_f', 'EMG2fmean', ...
                   'RESPmean', 'RESPstd', 'RESPmedian', 'RESPvar', ...
                   'RESPmean_d', 'RESPstd_d', 'RESPmedian_d', 'RESPvar_d', ...
                   'RESPmean_2d', 'RESPstd_2d', 'RESPmedian_2d', 'RESPvar_2d', ...
                   'RESPdom_f', 'RESPdom2_f', 'RESPfmean', ...
                   'TEMPmean', 'TEMPstd', 'TEMPmedian', 'TEMPvar', ...
                   'TEMPmean_d', 'TEMPstd_d', 'TEMPmedian_d', 'TEMPvar_d', ...
                   'TEMPmean_2d', 'TEMPstd_2d', 'TEMPmedian_2d', 'TEMPvar_2d', ...
                   'TEMPdom_f', 'TEMPdom2_f', 'TEMPfmean'};

time = chosen_dataset{1};
game = chosen_dataset{2};
subjects = chosen_dataset{3};
if reset_test_train
    test_split_percent = .5;

    [train_sub,test_sub] = crossvalind('HoldOut', length(subjects), test_split_percent);
end;


%% Data Setup
dataset_tuples_train = cell(sum(train_sub),1);
dataset_tuples_test = cell(sum(test_sub),1);
tic
train_iter = 1; test_iter = 1;
for i=1:length(subjects)
    if(train_sub(i))
        dataset_tuples_train{train_iter} = {time,game,subjects(i)};
        train_iter = train_iter+1;
    else
        dataset_tuples_test{test_iter} = {time,game,subjects(i)};
        test_iter = test_iter+1;
    end;
end;

% construct main data set
[trainX,trainY] = emotion_construct_large_dataset(dataset_tuples_train,0);
[testX,testY] = emotion_construct_large_dataset(dataset_tuples_test,0);
% add time feature
[trainX_time,trainY_time] = emotion_construct_large_dataset(dataset_tuples_train,1);
[testX_time,testY_time] = emotion_construct_large_dataset(dataset_tuples_test,1);
% z-scores
[ztrainX,muTrainX,sigmaTrainX] = zscore(trainX);
[ztestX,muTestX,sigmaTestX] = zscore(testX);
[ztrainY,muTrainY,sigmaTrainY] = zscore(trainY);
[ztestY,muTestY,sigmaTestY] = zscore(testY);
[ztrainX_time,muTrainX_time,sigmaTrainX_time] = zscore(trainX_time);
[ztestX_time,muTestX_time,sigmaTestX_time] = zscore(testX_time);


%% PCA(50)
[prin_comps,score,ev] = princomp(ztrainX);
[prin_comps_time,score_time,ev_time] = princomp(ztrainX_time);
if 0
    figure
    ev_plot = ev;
    plot(1:length(ev_plot),cumsum(ev_plot)/sum(ev_plot),'-bo');
    xlabel('Number of PCA components');
    ylabel('Explanation of Percentage of Variance in Y');
end;
ncomp = 50;
pcm = @(X) X*prin_comps(:,1:ncomp);
pcm_time = @(X) X*prin_comps_time(:,1:ncomp);
ptrainX = pcm(ztrainX);
ptestX = pcm(ztestX);
ptrainX_time = pcm_time(ztrainX_time);
ptestX_time = pcm_time(ztestX_time);

dataset_construction_time = toc


%% machine setup
machine_sets = {};
set_row = @(name,b,s) {name,b{1},b{2},s};


%% run regular
[mset,best] = emotion_analysis_run_machines(trainX,trainY,testX,testY);
machine_sets{end+1} = set_row('reg',best,mset);
[mset,best] = emotion_analysis_run_machines(trainX_time,trainY,testX_time,testY);
machine_sets{end+1} = set_row('reg_time',best,mset);

% %% run z-score
% [mset,best] = emotion_analysis_run_machines(ztrainX,trainY,ztestX,testY);
% machine_sets{end+1} = set_row('zscore',best,mset);
% [mset,best] = emotion_analysis_run_machines(ztrainX_time,trainY,ztestX_time,testY);
% machine_sets{end+1} = set_row('zscore_time',best,mset);


% %% run pca
% [mset,best] = emotion_analysis_run_machines(ptrainX,trainY,ptestX,testY);
% machine_sets{end+1} = set_row('pca',best,mset);
% [mset,best] = emotion_analysis_run_machines(ptrainX_time,trainY,ptestX_time,testY);
% machine_sets{end+1} = set_row('pca_time',best,mset);


%% show results
for i=1:length(machine_sets)
    fprintf('%s\t%s %0.6f\n',machine_sets{i}{1},machine_sets{i}{2},machine_sets{i}{3});
    mset = machine_sets{i}{4};
    for j=1:length(mset)
        fprintf('  %s %0.6f\n',mset{j,1},mset{j,2});
    end
end;
