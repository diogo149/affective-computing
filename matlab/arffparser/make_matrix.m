for iter=1:43
%     attributes=arffparser('read','sub1-escape_emotion');
    filepath=
    filename=strcat('sub',num2str(iter),'-escape_emotion');
    attributes=arffparser('read',filename);
    
    attr_names = fieldnames(attributes);
    numattr=length(attr_names);
    numtimepts=length(attributes.(char(names(1))).values)
    X=zeros(numtimepts,numattr-1);
    for i=1:numattr-1
        X(:,i)=(attributes.(char(names(i))).values)';
    end
    label=(attributes.(char(names(numattr))).values)';
    distlabel=unique(label);

    colorMap=containers.Map;
    choiceofcolors=[1 0 0; 0 1 0; 0 0 1];
    for l=1:length(distlabel)
        colorMap(char(distlabel(l)))=choiceofcolors(l,:);
    end

    %Do SNe, get mapped coords
    ydata = tsne(X(:,1:60));


    hold on;
    %to change color doing a for loop for scatter plot
    for i=1:numtimepts
        hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorMap(char(label(i))),'MarkerFaceColor', colorMap(char(label(i))));
    end
    axis off;
    legend(hd,distlabel);
    hold off;
    saveas(gcf,strcat('images/',filename,'.png'));
    close();
end