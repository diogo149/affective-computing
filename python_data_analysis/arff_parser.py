from scipy.io.arff import loadarff
import numpy as np
from emotion_to_num import emotion_to_num


def arff_parser(filename):
    return loadarff(filename)


# this should be 30 nans for each data set
def count_nan(x):
    nans = 0
    for count in range(len(x)):
        if not isinstance(x[count], np.string_) and np.isnan(x[count]):
            nans += 1
    return nans


def full_arff_parser(filename, dimensions=90):
    # only taking the first element of the tuple because the 2nd is metadata
    arff_data = arff_parser(filename)[0]
    rows = len(arff_data)
    Y = np.empty(rows)
    X = np.empty((rows, dimensions), dtype=np.float32)
    for row in range(rows):
        emotion = arff_data[row][-1]
        if arff_data[row][dimensions] != emotion:
            raise Exception("dimension incorrect for full_arff_parser")
        Y[row] = emotion_to_num(emotion)
        for col in range(dimensions):
            X[row, col] = arff_data[row][col]
    return X, Y

if __name__ == "__main__":

    if 0:
        filename = "../individual data half second/sub20-sahara_emotion.arff"
        filename = "../individual data 2s/sub20-escape_emotion.arff"
        x = arff_parser(filename)
        print x[0].shape
        print x[1]
        emotions = set(emotion_to_num(x[0][i][-1]) for i in range(x[0].shape[0]))
        print emotions
        print len(x[0][0])
        temp = x[0][0]
        print temp[-1].__class__
        print set([count_nan(x[0][count]) for count in range(len(x[0]))])

        X, Y = full_arff_parser(filename)
        print Y.shape
        print X.shape

    if 0:
        all_emotions = set()
        for i in range(1, 44):
            filename = "../individual data 2s/sub%d-escape_emotion.arff" % i
            filename = "../individual data half second/sub%d-escape_emotion.arff" % i
            filename = "../individual data half second/sub%d-sahara_emotion.arff" % i
            try:
                x = arff_parser(filename)
            except:
                print "missing: " + filename
                continue

            if 0:
                print set([count_nan(x[0][count]) for count in range(len(x[0]))])

            if 0:
                emotions = set(x[0][i][-1] for i in range(x[0].shape[0]))
                all_emotions |= emotions

        if all_emotions:  # all_emotions = set(['anxiety', 'challenged', 'curiosity', 'bored', 'frustrated', 'happy'])
            print all_emotions

    # this section makes the csv files
    if 1:
        for folder in ['2s', 'half second']:
            for game in ['escape', 'sahara']:
                for subject in range(1, 44):
                    filename = "../individual data %s/sub%d-%s_emotion.arff" % (folder, subject, game)
                    try:
                        X, Y = full_arff_parser(filename)
                    except:
                        print "missing or error at: " + filename
                        continue
                    folder_name = folder
                    if folder_name == "half second":
                        folder_name = "0.5s"
                    csvX = "../data/%s_%s_sub_%02d_X.csv" % (folder_name, game, subject)
                    csvY = "../data/%s_%s_sub_%02d_Y.csv" % (folder_name, game, subject)
                    np.savetxt(csvX, X, delimiter=",")
                    np.savetxt(csvY, Y, delimiter=",")

    # view number of observations for each
    if 0:
        for folder in ['2s', 'half second']:
            for game in ['escape', 'sahara']:
                for subject in range(1, 44):
                    filename = "../individual data %s/sub%d-%s_emotion.arff" % (folder, subject, game)
                    try:
                        X, Y = full_arff_parser(filename)
                    except:
                        print "missing or error at: " + filename
                        continue
                    print X.shape
