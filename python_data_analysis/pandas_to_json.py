# pandas to json
# note: this didn't work at all

import ujson as json


def pandas_to_json(dataframe, filename):
    d = [
        dict([
            (colname, row[i])
            for i, colname in enumerate(dataframe.columns)
        ])
        for row in dataframe.values
    ]
    with open(filename, 'w') as infile:
        infile.write(json.dumps(d))
