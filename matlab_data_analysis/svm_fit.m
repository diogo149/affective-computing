function [ yfit ] = svm_cv_train( xtrain,ytrain,xtest,rbf_sigma,boxconstraint )
    svmStruct = svmtrain(xtrain,ytrain,'Kernel_Function','rbf',...
   'rbf_sigma',rbf_sigma,'boxconstraint',boxconstraint);
    yfit = svmclassify(svmStruct,xtest);
end

