%% Data Import
games = {'sahara','escape'};
feature_signals = {{'EMG1',[3,49]},{'EMG2',[50,96]}};
subject_column = 1;
emotion_column = 2;

Emotion_Names = {'curiosity','anxiety','bored','interest','challenged','suspense',...
    'happy','hope','frustrated','anger', 'confused', 'excited', 'enjoy', ...
    'dispointed','surprised','relaxed', 'intrigued', 'anoyed', 'relieved',...
    'determined','irritated','neutral','regret','interested','amused',...
    'hesitate','mad'}; 

tian_v1_data = containers.Map;
for i=1:numel(games)
    game = games{i};
    game_map = containers.Map;
    
    load(sprintf('%s_1_20_v5_new_old.mat',game));
    full_dataset = new;
    load(sprintf('%s_21_43_v5_new_old.mat',game));
    full_dataset = [full_dataset; new];
    clear new;
    
    game_map('emotions') = full_dataset(:,emotion_column);
    game_map('emotion_names') = Emotion_Names;
    game_map('subjects') = full_dataset(:,subject_column);
    
    features = containers.Map;
    for j=1:numel(feature_signals)
        feature_map = containers.Map;
        signal = feature_signals{j};
        feature = signal{1};
        feature_indices = signal{2};
        
        feature_data = full_dataset(:,feature_indices(1):feature_indices(2));
        feature_map('data') = feature_data;
        
        feature_ranks = csvread(sprintf('%s_rank.csv',feature));
        feature_ranks = feature_ranks - min(feature_ranks) + 1;
        feature_map('ranks') = feature_ranks;
        
        features(feature) = feature_map;
    end;
    game_map('features') = features;
    
    tian_v1_data(game) = game_map;
end;

save('new_data/tian_v1_data.mat','tian_v1_data');
