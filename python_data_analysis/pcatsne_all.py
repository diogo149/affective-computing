from transform_data import transform_all, transform_data
from bhtsne import bh_tsne
from numpy import array
from sklearn.decomposition import RandomizedPCA as PCA

default_perplexity = 30


def pcatsne_fit_transform(X):
    pca = PCA(n_components=50)
    X = pca.fit_transform(X)
    X_list = list(X)
    output = [result for result in bh_tsne(X_list, verbose=False, perplexity=default_perplexity)]
    return array(output)


transform_name = "pcatsne"
transform = pcatsne_fit_transform


def pcatsne_all():
    transform_all(pcatsne_fit_transform, transform_name + "_" + str(default_perplexity))


def pcatsne_one(folder_name, game, subject):
    transform_data(folder_name, game, subject, transform, transform_name)

if __name__ == "__main__":
    if 1:
        pcatsne_all()
    if 0:
        pcatsne_one("2s", "escape", 8)
