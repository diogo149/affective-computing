# removes columsn with nan or infinity

from numpy import any, isnan, isinf


def clean_columns(X):
    clean = []
    columns = X.shape[1]
    for col in range(columns):
        current_column = X[:, col]
        if not(any(isnan(current_column)) or any(isinf(current_column))):
            clean.append(col)
    return X[:, clean]
