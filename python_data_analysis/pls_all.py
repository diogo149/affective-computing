from transform_data import transform_all, transform_data
from filter_rows import filter_rows

from sklearn.pls import PLSRegression


def pls_fit_transform(X):
    clf = PLSRegression(n_components=2)
    clf.fit(X)
    # clf.fit_transform(X)
    return clf.transform(X)  # does not work; PLS takes a Y


transform_name = "pls"


def pls_all():
    transform_all(pls_fit_transform, transform_name)


def pls_one(folder_name, game, subject):
    transform_data(folder_name, game, subject, pls_fit_transform, transform_name)


def pls_filter_fit_transform(X):
    Y = filter_rows(X, kept_features=["mean", "std", "median", "var", "dom_f", "dom2_f", "fmean"])
    return pls_fit_transform(Y)


def pls_all_filter():
    transform_all(pls_filter_fit_transform, transform_name + "_filter")

if __name__ == "__main__":
    if 0:
        pls_all()
    if 0:
        pls_one("2s", "escape", 8)
    if 1:
        pls_all_filter()
