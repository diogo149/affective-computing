function [ machine_metrics, best_metric ] = emotion_analysis_run_machines( trainX, trainY, testX, testY )
%% init
machine_predictions = {};
knn = @(trX,trY,teX,k) knnclassify(teX,trX,trY,k);
ovo = 1;

%% Random Forests
if 0
    tic;
    for e=[25,50,100]
        RF_fit = TreeBagger(e,trainX,trainY,'Method','classification','OOBPred','on');
        pred = predict(RF_fit,testX);
        pred = cellfun(@(s) [str2num(s)],pred);
        RF_time = {e,toc}
        machine_predictions{end+1} = {sprintf('Random Forest (%d)',e),pred};
    end
end;

%% K-NNs
if 1
    for k=[1,3,9,21,81,243,729]
        tic;
        knnfit = @(a,b,c) knn(a,b,c,k);
        if ovo
            pred = one_vs_one_fit(knnfit,trainX,trainY,testX);
        else
            pred = knnfit(trainX,trainY,testX);
        end;
        KNN_time = {k,toc}
        machine_predictions{end+1} = {sprintf('K-NN(%d)',k),pred};
    end
end;

%% OVO RBF-SVM
if 0
    tic;
    svm = @(trainX,trainY,testX) svm_fit(trainX,trainY,testX,1,1);
    pred = one_vs_one_fit(@svm_cv_fit,trainX,trainY,testX);
    SVM_time = toc
    machine_predictions{end+1} = {'OVO RBF-SVM',pred};
end;

%% PLS(30)
if 0
    pls_ncomp = 60;
    [XL,YL,XS,YS,BETA,PCTVAR,MSE,stats] = plsregress(trainX,trainY,pls_ncomp);
    figure;
    plot(1:ncomp,cumsum(100*PCTVAR(2,:)),'-bo');
    xlabel('Number of PLS components');
    ylabel('Percent Variance Explained in Y');
end;

%% Machine Evaluations
error_metric = @(hyp_y,y) sum(hyp_y == y)/length(y);

machine_metrics = cell(length(machine_predictions),2);
for i=1:length(machine_predictions)
    mp = machine_predictions{i};
    machine_metrics{i,1} = mp{1};
    machine_metrics{i,2} = error_metric(mp{2},testY);
end;
machine_metrics = sortrows(machine_metrics,2);
best_metric = machine_metrics(end,:);
end

