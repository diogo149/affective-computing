from pandas import DataFrame
from load_data import load_data
from plot_transformed import plot_transformed


def save_data(data, folder_name, game, subject, category, saveplot=False):
    assert folder_name in ['2s', '0.5s']
    assert game in ['escape', 'sahara']
    assert subject in range(1, 44)
    filename = "../%s/%s_%s_sub_%02d.csv" % (category, folder_name, game,
                                             subject)
    DataFrame(data).to_csv(filename, header=False, index=False)
    if saveplot:
        Y = load_data(folder_name, game, subject, x=False)
        filename2 = "../%s/figures/%s_%s_sub_%02d.png" % \
            (category, folder_name, game, subject)
        plot_transformed(data, Y, filename2)
