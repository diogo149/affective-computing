function [ pred ] = svm_cv_fit(trainX,trainY,testX)
    c = cvpartition(size(trainX,1),'kfold',2);
    minfn = @(z)crossval('mcr',trainX,trainY,'Predfun', ...
                @(xtrain,ytrain,xtest)svm_fit(xtrain,ytrain, xtest,exp(z(1)),exp(z(2))), ...
                'partition',c);
    opts = optimset('TolX',5e-2,'TolFun',5e-2);
    [searchmin fval] = fminsearch(minfn,linspace(-1,1,2),opts);
    z = exp(searchmin);
    svmStruct = svmtrain(trainX,trainY,'Kernel_Function','rbf','rbf_sigma',z(1),'boxconstraint',z(2),'showplot',true);
    pred = svmclassify(svmStruct,testX);
end

