# note this does not work

from transform_data import transform_all


def tsne_all():
    # from calc_tsne import calc_tsne
    # transform_all(lambda data: calc_tsne(data), "tsne")
    from tsne import tsne
    transform_all(lambda data: tsne(data), "tsne")

if __name__ == "__main__":
    tsne_all()
