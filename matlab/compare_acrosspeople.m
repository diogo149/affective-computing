function []=compare_acrosspeople(game,subj1,subj2)
    %set up color map
    emotions={'frustrated','curiosity','bored','anxiety','interest','challenged','suspense','happy','hope','anger','confused','excited','enjoy','dispointed','surprise','relaxed','intrigued','anoyed','relieved','determined','irritated','neutral','regret','interested','amused','hesitate','mad'};
    emotions1=strcat(num2str(subj1),'-',emotions);
    emotions2=strcat(num2str(subj2),'-',emotions)
    colorHash=containers.Map;
    choiceofcolors=[1 0 0; 0 0 1; 0 1 0; 0 1 1; 0 0 0; 1 0 1; 1 1 0; 0.5, 0.5, 0.5; 0 0 0.5; 0 0.5 0; 0 0.5 0.5; 0.5 0 0; 0.5 0 0.5; 0.5 0.5 0;];
    for l=1:length(choiceofcolors)
        colorHash(char(emotions1(l)))=choiceofcolors(l,:);
        colorHash(char(emotions2(l)))=choiceofcolors(l,:);
    end
    %same person across games
 
        % Load subj1 data
        load(strcat('2s_',game,'/X',num2str(subj1)));
        load(strcat('2s_',game,'/label',num2str(subj1)));
        X1=X;
        origlabel1=label;
        label1=strcat(num2str(subj1),'-',label)%needed this coz struct was saved
        num1=size(X,1);
        clear X;
        clear label;

        % Load subj2 data
        load(strcat('2s_',game,'/X',num2str(subj2)));
        load(strcat('2s_',game,'/label',num2str(subj2)));
        X2=X;
        origlabel2=label;
        label2=strcat(num2str(subj2),'-',label)%needed this coz struct was saved
        num2=size(X,1);
        clear X;
        clear label;

        %find SNE data
        X=[X1;X2];
        label=[label1;label2];
        origlabel=[origlabel1;origlabel2];
        ydata = tsne(X(:,1:60));
        distlabel=unique(label);

        %Draw image
        figure('Visible','off');
        hold on;
        %to change color doing a for loop for scatter plot
        for i=1:num1
            hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorHash(char(label(i))),'MarkerFaceColor', colorHash(char(label(i))));
        end

        for i=num1+1:num1+num2
            hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorHash(char(label(i))),'^','MarkerFaceColor', colorHash(char(label(i))));
        end
        axis off;
        legend(hd,unique(label),'Location','BestOutside');
        hold off;
        saveas(gcf,strcat('2s_acrosspeople/',game,'subject',num2str(subj1),'_',num2str(subj2),'.png'));
        clear hd;
        close();  

 
end