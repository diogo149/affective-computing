# 'anxiety': blue
# 'challenged': magenta
# 'curiosity': yellow
# 'bored': grey
# 'frustrated': red
# 'happy': yellow


def num_to_color(num):
    return ('', 'b', 'm', 'y', '0.75', 'r', 'y')[int(num)]
