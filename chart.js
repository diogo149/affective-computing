// Various accessors that specify the dimensions of data to visualize

function x(d) {
    return d.q1;
}

function y(d) {
    return d.q2;
}

function radius(d) {
    return d.time_radius;
}

function color(d) {
    return d.emotion;
}

function key(d) {
    return d.abs_diff_time;
}

// Chart dimensions.
var margin = {
    top: 50,
    right: 50,
    bottom: 50,
    left: 50
},
width = window.innerWidth - margin.right - margin.left,
    height = window.innerHeight - margin.top - margin.bottom;

var viz_emotions = function(data_folder, trans, time_str, game, sub) {

    // construct minutes:seconds given a time slice in the viz
    function to_time_string(time_slice) {
        var d = new Date(time_slice * parseFloat(time_str) * 1000);
        return "" + d.getMinutes() + ":" + d.getSeconds();
    }

    var file_name = trans + "_" + time_str + "s_" + game + "_sub_" + sub + ".csv";
    var file_path = "./" + data_folder + "/" + file_name;
    // construct the viz
    d3.csv(file_path, function(points) {
        if(!points) {
            alert("Cannot find file " + file_name);
            return;
        }
        // csvs read in as strings
        emotion_set = {};
        points.forEach(function(d) {
            d.q1 = parseFloat(d.q1);
            d.q2 = parseFloat(d.q2);
            emotion_set[d.Emotion] = 0;
        });

        // constants
        var TIME_DURATION = 25*1000*2/parseFloat(time_str); // milliseconds duration of motion viz
        var MIN_TIME = 1; // starting time slice
        var MAX_TIME = points.length; // ending time slice
        var TIME_TRAIL = 100; // number of points in temporal trail
        var TIME_TRAIL_MIN = 3; // cut points in trail, ending trail

        // calculate data range
        var rangeX = [Number.MAX_VALUE, -Number.MAX_VALUE];
        var rangeY = [Number.MAX_VALUE, -Number.MAX_VALUE];
        points.forEach(function(d) {
            var px = x(d);
            var py = y(d);
            rangeX[0] = Math.min(rangeX[0], px);
            rangeX[1] = Math.max(rangeX[1], px);
            rangeY[0] = Math.min(rangeY[0], py);
            rangeY[1] = Math.max(rangeY[1], py);
        });

        var xScale = d3.scale.linear().domain(rangeX).range([0, width]),
            yScale = d3.scale.linear().domain(rangeY).range([height, 0]),
            radiusScale = d3.scale.pow().domain([TIME_TRAIL_MIN, TIME_TRAIL]).range([3, 20]),
            colorScale = d3.scale.category10();

        all_emotions = ['anxiety', 'happy', 'curiosity','frustrated','challenged' , 'bored'];
        all_emotions.forEach( function(e) {
            colorScale(e);
        });

        // rangeX[0] -= 1;
        // rangeX[1] += 1;
        // rangeY[0] -= 1;
        // rangeY[1] += 1;

        // // The x & y axes.
        // var xAxis = d3.svg.axis().orient("bottom").scale(xScale).ticks(12, d3.format(",d")),
        //     yAxis = d3.svg.axis().scale(yScale).orient("left");

        // Create the SVG container and set the origin.
        d3.select("#chart").select("svg").remove();
        var svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // add legend
        startx = width-10;
        starty = 0;
        for(var emotion in emotion_set) {
            svg.append("text")
                        .attr("class", "legend label")
                        .attr("text-anchor", "end")
                        .attr("y", starty)
                        .attr("x", startx)
                        .text(emotion);
            svg.append("circle")
                .style("stroke", "gray")
                .style("fill", colorScale(emotion))
                .attr("r", 4)
                .attr("cx", startx+6)
                .attr("cy", starty-4);
            starty+=10;
        }

        // // Add the x-axis.
        // svg.append("g")
        //     .attr("class", "x axis")
        //     .attr("transform", "translate(0," + height + ")")
        //     .call(xAxis);

        // // Add the y-axis.
        // svg.append("g")
        //     .attr("class", "y axis")
        //     .call(yAxis);

        // // Add an x-axis label.
        // svg.append("text")
        //     .attr("class", "x label")
        //     .attr("text-anchor", "end")
        //     .attr("x", width)
        //     .attr("y", height - 6)
        //     .text("q1");

        // // Add a y-axis label.
        // svg.append("text")
        //     .attr("class", "y label")
        //     .attr("text-anchor", "end")
        //     .attr("y", 6)
        //     .attr("dy", ".75em")
        //     .attr("transform", "rotate(-90)")
        //     .text("q2");

        // Add the time label; the value is set on transition.
        var label = svg.append("text")
            .attr("class", "time label")
            .attr("text-anchor", "end")
            .attr("y", height - 24)
            .attr("x", width)
            .text("0000000");

        // Add the title.
        var title = svg.append("text")
            .attr("class", "graph_title label")
            .attr("text-anchor", "middle")
            .attr("y", 0)
            .attr("x", width/2)
            .text("subject "+sub+" - "+game+" - "+trans);


        // Positions the dots based on data.

        function dot_draw(dot) {
            var opaScale = d3.scale.pow()
                .domain([TIME_TRAIL_MIN, TIME_TRAIL])
                .range([0.6, 1]);
            dot.attr("cx", function(d) {
                return xScale(x(d));
            })
                .attr("cy", function(d) {
                return yScale(y(d));
            })
                .attr("r", function(d) {
                return radiusScale(radius(d));
            })
                .style("fill", function(d) {
                return colorScale(color(d));
            })
                .style("opacity", function(d) {
                return opaScale(radius(d));
            });
        }

        function dot_draw_hover(dot) {
            var opaScale = d3.scale.pow()
                .domain([TIME_TRAIL_MIN, TIME_TRAIL])
                .range([0.6, 1]);
            dot.attr("cx", function(d) {
                return xScale(x(d));
            })
                .attr("cy", function(d) {
                return yScale(y(d));
            })
                .attr("r", function(d) {
                return radiusScale(TIME_TRAIL);
            })
                .style("fill", function(d) {
                return colorScale(color(d));
            })
                .style("opacity", function(d) {
                return opaScale(TIME_TRAIL);
            });

        }

        // dot mouseovers
        function dot_over(d, i) {
            dot_draw_hover(d3.select(this)
                .transition().duration(50));
        }

        function dot_out(dot, i) {
            dot_draw(d3.select(this)
                .transition().duration(50));
        }

        // Add a dot per point. Initialize the data at beginning time slice, and set the colors.
        var dot = svg.append("g")
            .attr("class", "dots")
            .selectAll(".dot")
            .data(interpolateData(MIN_TIME))
            .enter().append("circle")
            .attr("class", "dot")
            .on("mouseover", dot_over)
            .on("mouseout", dot_out)
            .style("fill", function(d) {
            return colorScale(color(d));
        })
            .call(dot_draw)
            .sort(order);

        // Add a title.
        dot.append("title").text(function(d) {
            return d.emotion + " at " + to_time_string(d.index);
        });

        // Add an overlay for the time label.
        var box = label.node().getBBox();

        var overlay = svg.append("rect")
            .attr("class", "overlay")
            .attr("x", box.x)
            .attr("y", box.y)
            .attr("width", box.width)
            .attr("height", box.height)
            .on("mouseover", enableInteraction);

        // Start a transition that interpolates the data based on time.
        svg.transition()
            .duration(TIME_DURATION)
            .ease("linear")
            .tween("time", tweenTime)
            .each("end", enableInteraction);


        // Defines a sort order so that the smallest dots are drawn on top.

        function order(a, b) {
            return radius(a) - radius(b);
        }

        // After the transition finishes, you can mouseover to change the time.

        function enableInteraction() {
            var timeScale = d3.scale.linear()
                .domain([MIN_TIME, MAX_TIME])
                .range([box.x + 10, box.x + box.width - 10])
                .clamp(true);

            // Cancel the current transition, if any.
            svg.transition().duration(0);

            overlay.on("mouseover", mouseover)
                .on("mouseout", mouseout)
                .on("mousemove", mousemove)
                .on("touchmove", mousemove);

            function mouseover() {
                label.classed("active", true);
            }

            function mouseout() {
                label.classed("active", false);
            }

            function mousemove() {
                displayTime(timeScale.invert(d3.mouse(this)[0]));
            }
        }

        // Tweens the entire chart by first tweening the time, and then the data.
        // For the interpolated data, the dots and label are redrawn.

        function tweenTime() {
            var time = d3.interpolateNumber(MIN_TIME, MAX_TIME + TIME_TRAIL);
            return function(t) {
                displayTime(time(t));
            };
        }

        // Updates the display to show the specified time.
        // also setups the time's data
        function displayTime(time_slice) {
            dot.data(interpolateData(time_slice)).call(dot_draw).sort(order);
            if(time_slice<=MAX_TIME+2)
                label.text(to_time_string(time_slice));
        }

        // Interpolates the dataset for the given (fractional) time.

        function interpolateData(time) {
            var t = 0; // data index
            return points.map(function(d) {
                t++;
                // time_diff represents order in trail
                // bigger means front of tail
                var time_diff = TIME_TRAIL_MIN;
                var diff = TIME_TRAIL - (time - t);
                if (t <= time && diff >= TIME_TRAIL_MIN) {
                    time_diff = diff;
                }
                // show data as time unwinds
                var q1 = 0;
                var q2 = 0;
                if (t <= time) {
                    q1 = d.q1;
                    q2 = d.q2;
                }
                return {
                    emotion: d.Emotion, // pull emotion from csv
                    q1: q1,
                    q2: q2,
                    index: t,
                    // Sets the radius size for given time away from current time.
                    time_radius: Math.round(time_diff)
                };
            });
        }

    });
};
