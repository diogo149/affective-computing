function [attrnum]=attributestouse(feature)
iter=1
filepath='individual data 2s/';
filename=strcat('sub',num2str(iter),'-sahara_emotion');
attributes=arffparser('read',strcat(filepath,filename));
names=fieldnames(attributes);
mvinds=strfind(names,feature);
attrnum=[];
for i=1:length(names)
    if(isequal(mvinds(i),{[]}))
%         not a mean value field
    else
        %All mean values are in attrnum indices
        attrnum=[attrnum;i];
    end
end
end