from transform_data import transform_all


def ica_all():
    from sklearn.decomposition import FastICA
    ica = FastICA(n_components=2)
    transform_all(ica.fit_transform, "ica")

if __name__ == "__main__":
    ica_all()
