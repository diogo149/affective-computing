import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def get_filename(game, window):
    assert game in games
    assert window in windows
    return "../new_data/error_%s_%d.csv" % (game, window)


def confusion_matrix(df, name):
    arr = df.as_matrix()
    fig = plt.figure()
    plt.clf()
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    res = ax.imshow(np.array(arr), cmap=plt.cm.jet, interpolation='nearest')
    cb = fig.colorbar(res)
    plt.xticks(range(df.shape[1]), df.columns, rotation='vertical')
    plt.yticks(range(df.shape[0]), df.index)
    plt.savefig(name + '.png', format='png')
    plt.close()

if __name__ == "__main__":
    games = ["sahara", "escape"]
    windows = [2, 1]
    name = "../new_data/error_sahara_1.csv"
    for game in games:
        for window in windows:
            name = get_filename(game, window)
            print name
            confusion_matrix(pd.read_csv(name, index_col=0), name)
