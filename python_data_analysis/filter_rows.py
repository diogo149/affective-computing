from numpy import delete

col_dict = ["mean", "std", "median", "var",
            "mean_d", "std_d", "median_d", "var_d",
            "mean_2d", "std_2d", "median_2d", "var_2d",
            "dom_f", "dom2_f", "fmean"]


def filter_rows(X, removed_features=None, kept_features=None):
    if kept_features is not None:
        removed_features = [x for x in col_dict if x not in kept_features]
    # assuming X is a numpy matrix
    n = X.shape[1]
    remove_cols = []
    for removed_feature in removed_features:
        index = col_dict.index(removed_feature)
        remove_cols += [index + offset for offset in range(0, n, 15)]
    return delete(X, remove_cols, 1)
