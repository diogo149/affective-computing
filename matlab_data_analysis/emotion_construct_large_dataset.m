function [ X, Y ] = emotion_construct_large_dataset(emotion_files_tuples,add_time_feature)
    file_tuple = emotion_files_tuples{1};
    remove_30_nans = 1;
    init = 0;
    for i=1:size(emotion_files_tuples,1)
        file_tuple = emotion_files_tuples{i};
        [TX,TY] = emotion_get_data(file_tuple{1},file_tuple{2},file_tuple{3},remove_30_nans);
        if( sum(isnan(TX(:)))==0 )
            if ~init
                if add_time_feature
                    X = emotion_data_add_time_track(TX);
                else
                    X = TX;
                end;
                Y = TY;
                init = 1;
            else
                if add_time_feature
                    X = [X; emotion_data_add_time_track(TX)];
                else
                    X = [X; TX];
                end;
                Y = [Y; TY];
            end;
        end;
    end;
end

