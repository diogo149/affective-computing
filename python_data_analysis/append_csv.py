from pandas.io.parsers import read_csv
from emotion_to_num import num_to_emotion
from detect_outliers import detect_outliers


def append_csv(category, folder_name, game, subject, remove_outliers=0):
    assert folder_name in ['2s', '0.5s']
    assert game in ['escape', 'sahara']
    assert subject in range(1, 44)
    filenameX = "../%s/%s_%s_sub_%02d.csv" % (category, folder_name, game,
                                              subject)
    filenameY = "../data/%s_%s_sub_%02d_Y.csv" % (folder_name, game, subject)
    X = read_csv(filenameX, header=None)
    emotion_num = read_csv(filenameY, header=None).as_matrix()[:, 0]
    Y = [num_to_emotion(x) for x in list(emotion_num)]
    column_labels = ["q1", "q2"]
    X.columns = column_labels
    X["Emotion"] = Y
    if remove_outliers:
        valid = detect_outliers(X[column_labels].as_matrix())
        X = X.irow(valid)
    filename_out = "../output/%s_%s_%s_sub_%02d.csv" % (category, folder_name, game,
                                                        subject)
    X.to_csv(filename_out, header=True, index=False)

if __name__ == "__main__":
    if 0:
        from sys import argv
        append_csv(argv[1], argv[2], argv[3], int(argv[4]), 0)
    if 1:
        from sys import argv
        category = argv[1]
        for folder_name in ['2s', '0.5s']:
            for game in ['escape', 'sahara']:
                for subject in range(1, 44):
                    try:
                        append_csv(category, folder_name, game, subject)
                    except IOError:
                        pass
    if 0:
        append_csv("kpca_rbf", "0.5s", "sahara", 15, remove_outliers=0.01)
