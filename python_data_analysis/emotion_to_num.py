# emotion map
# among the data points given, only 6 emotions were shown: 'anxiety',
# 'challenged', 'curiosity', 'bored', 'frustrated', 'happy'
# this script maps each emotion to a number
# 'anxiety': 1
# 'challenged': 2
# 'curiosity': 3
# 'bored': 4
# 'frustrated': 5
# 'happy': 6

emotion_dict = {'anxiety': 1, 'challenged': 2, 'curiosity': 3, 'bored': 4,
                'frustrated': 5, 'happy': 6}

num_dict = dict([(value, key) for key, value in emotion_dict.items()])


def emotion_to_num(emotion):
    return emotion_dict[emotion]


def num_to_emotion(num):
    return num_dict[num]
