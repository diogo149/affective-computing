%set up color map
emotions={'frustrated','curiosity','bored','anxiety','interest','challenged','suspense','happy','hope','anger','confused','excited','enjoy','dispointed','surprise','relaxed','intrigued','anoyed','relieved','determined','irritated','neutral','regret','interested','amused','hesitate','mad'};Sahara_emotions={'Sahara_frustrated','Sahara_curiosity','Sahara_bored','Sahara_anxiety','Sahara_interest','Sahara_challenged','Sahara_suspense','Sahara_happy','Sahara_hope','Sahara_anger','Sahara_confused','Sahara_excited','Sahara_enjoy','Sahara_dispointed','Sahara_surprise','Sahara_relaxed','Sahara_intrigued','Sahara_anoyed','Sahara_relieved','Sahara_determined','Sahara_irritated','Sahara_neutral','Sahara_regret','Sahara_interested','Sahara_amused','Sahara_hesitate','Sahara_mad'};
colorHash=containers.Map;
choiceofcolors=[1 0 0; 0 0 1; 0 1 0; 0 1 1; 0 0 0; 1 0 1; 1 1 0; 0.5, 0.5, 0.5; 0 0 0.5; 0 0.5 0; 0 0.5 0.5; 0.5 0 0; 0.5 0 0.5; 0.5 0.5 0;];
for l=1:length(choiceofcolors)
    colorHash(char(emotions(l)))=choiceofcolors(l,:);
end
[meanvalueinds]=attributestouse('meanvalue');
meansonly=1;
game='escape'
opfilepath=strcat('2s_mean_',game);
for iter=2:43
%     attributes=arffparser('read','sub1-escape_emotion');
    filepath='individual data 2s/';
    filename=strcat('sub',num2str(iter),'-',game,'_emotion');
    attributes=arffparser('read',strcat(filepath,filename));
    
    attr_names = fieldnames(attributes);
    numattr=length(attr_names);
    numtimepts=length(attributes.(char(attr_names(1))).values)
    X=zeros(numtimepts,numattr-1);
    for i=1:numattr-1
        X(:,i)=(attributes.(char(attr_names(i))).values)';
    end
    label=(attributes.(char(attr_names(numattr))).values)';
    distlabel=unique(label);
    if(meansonly)
        %only take mean values
        X=X(:,meanvalueinds);
        X=X(:,1:4);
        ydata=tsne(X);
    else
        %Do SNe, get mapped coords
        ydata = tsne(X(:,1:60));
    end;
    
%save data for future use
    save(strcat(opfilepath,'/X',num2str(iter)),'X');
    save(strcat(opfilepath,'/label',num2str(iter)),'label');
    save(strcat(opfilepath,'/ydata',num2str(iter)),'ydata');

    
    %Draw image
    figure('Visible','off');
    hold on;
    %to change color doing a for loop for scatter plot
    for i=1:size(ydata,1)
        hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorHash(char(label(i))),'^','MarkerFaceColor', colorHash(char(label(i))));
    end
    axis off;
    legend(hd,distlabel,'Location','BestOutside');
    hold off;
    saveas(gcf,strcat(opfilepath,'/',filename,'.png'));
    clear hd;
    close();    
end