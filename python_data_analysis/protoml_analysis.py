from sklearn.decomposition import RandomizedPCA

from protoml.feature import *
from protoml.extras.tsne import BarnesHutSNE
from protoml.extras.visualization import plot2D

from subprocess import call
from os.path import exists

col_dict = ["mean", "std", "median", "var",
            "mean_d", "std_d", "median_d", "var_d",
            "mean_2d", "std_2d", "median_2d", "var_2d",
            "dom_f", "dom2_f", "fmean"]

col_dict2 = ["EMG", "SC", "BVP", "EMG2", "RESP", "TEMP"]

orig_cols = [x + y for x in col_dict2 for y in col_dict]


def make_folders(transform_name):
    directory = "../" + transform_name
    if not exists(directory):
        call(["mkdir", directory])


def make_file_name(category="data", folder_name="2s", game="escape", subject=1, Y=False):
    filename = "../%s/%s_%s_sub_%02d" % (category, folder_name, game, subject)
    if category == "data":
        if Y:
            filename += "_Y"
        else:
            filename += "_X"
    filename += ".csv"
    return filename

transform_name = "no_deriv"

bhtsne_ft = ("bhtsne", "[^emotion]", BarnesHutSNE(perplexity=5), True)
pca_ft = ("pca", "[^emotion]", RandomizedPCA(n_components=20))


def protoml_analysis(folder, game, subject):
    file_in = make_file_name("data", folder, game, subject)
    file_in_emotion = make_file_name("data", folder, game, subject, Y=True)
    file_out = make_file_name(transform_name, folder, game, subject)

    plot_ft = (None, ["bhtsne", "emotion"], lambda x: plot2D(x, file_out, True))

    ft = Feature()
    ft.add_transforms([
                      ft_read_csv(file_in, header=None, index_col=False),
                      ft_rename(orig_cols),
                      ft_rm(".*_2*d"),
                      ft_rm_nan_cols(),
                      ft_shape(),
                      ft_standardscaler(),
                      ft_read_csv(file_in_emotion, "emotion", header=None, index_col=False),
                      # problem on protoml_analysis('2s', 'escape', 6)
                      # ft_rm_outliers(percentile=0.01, multivariate=True),
                      ft_rename(["emotion"], -1),
                      # pca_ft,
                      # ft_labelbinarizer("bin_emo", "emotion", remove=False),
                      bhtsne_ft,
                      plot_ft,
                      ft_rm("emotion"),
                      ft_to_csv(file_out, header=False, index=False)
                      ])
    ft.fit()


if __name__ == "__main__":
    make_folders(transform_name)
    if 0:
        protoml_analysis('2s', 'escape', 6)

    if 1:
        for folder in ['2s', '0.5s']:
            for game in ['escape', 'sahara']:
                for subject in range(1, 44):
                    try:
                        protoml_analysis(folder, game, subject)
                    except IOError:
                        print "failure at %s %s %d %s" % (folder, game, subject, transform_name)


# use X and Y on emotion data (for outlier detection) so emotions aren't taken into account
