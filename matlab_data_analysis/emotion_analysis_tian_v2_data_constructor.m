%% Data Import
games = {'sahara','escape'};
windows = {'window1','window2'};
subject_column = 1;
emotion_column = 2;

Emotion_Names = {'curiosity','anxiety','bored','interest','challenged','suspense',...
    'happy','hope','frustrated','anger', 'confused', 'excited', 'enjoy', ...
    'dispointed','surprised','relaxed', 'intrigued', 'anoyed', 'relieved',...
    'determined','irritated','neutral','regret','interested','amused',...
    'hesitate','mad'}; 

tian_v2_data = containers.Map;
for i=1:numel(games)
    game = games{i};
    game_map = containers.Map;
    for j=1:numel(windows)
        window = windows{i};
        window_map = containers.Map;
    
        load(sprintf('%s_all_%s.mat',game,window));
        full_dataset = new;
        clear new;

        game_map('emotions') = full_dataset(:,emotion_column);
        game_map('emotion_names') = Emotion_Names;
        game_map('subjects') = full_dataset(:,subject_column);
        
        feature_ranks = csvread(sprintf('%s_rank.csv',window));
        features = full_dataset(:,feature_ranks);
        window_map('data') = features;
        window_map('ranks') = feature_ranks;
        
        game_map(window) = window_map;
    end;
    game_map('features') = features;
    tian_v2_data(game) = game_map;
end;

save('new_data/tian_v2_data.mat','tian_v2_data');
