%% Analysis Parameters
num_emotions = 5;
num_ranked_features = 30;
num_ranked_features = min(num_ranked_features,47);
test_split_percent = .5;
reset_train_test = 1;

game = 'sahara';
game = 'escape';
features = 'EMG1';

Emotion_Names = {'curiosity','anxiety','bored','interest','challenged','suspense',...
    'happy','hope','frustrated','anger', 'confused', 'excited', 'enjoy', ...
    'dispointed','surprised','relaxed', 'intrigued', 'anoyed', 'relieved',...
    'determined','irritated','neutral','regret','interested','amused',...
    'hesitate','mad'}; 

%% Data Import
if strcmp(game,'escape')
    load('escape_1_20_v5_new_old.mat');
    full_dataset = new;
    load('escape_21_43_v5_new_old.mat');
    full_dataset = [full_dataset; new];
    clear new;
else
    load('sahara_1_20_v5_new_old.mat');
    full_dataset = new;
    load('sahara_21_43_v5_new_old.mat');
    full_dataset = [full_dataset; new];
    clear new;
end

subject_column = 1;
emotion_column = 2;
if strcmp(features,'EMG1')
    emotion_features_start = 3;
    emotion_features_end = 49;
    feature_ranks = csvread('EMG1 rank.csv');
else
    emotion_features_start = 50;
    emotion_features_end = 96;
    feature_ranks = csvread('EMG2 rank.csv');
end;
feature_ranks = feature_ranks(1:num_ranked_features)

emotions = full_dataset(:,2);
Emotions_All = arrayfun(@(e) Emotion_Names(e),unique(emotions)')
all_emotions = unique(emotions);
emotions_hist = hist(emotions,all_emotions);
[sortedValues,sortIndex] = sort(emotions_hist,'descend');
all_emotion_ind = sortIndex(2:+num_emotions);
emotion_ind = arrayfun(@(e) any(all_emotions(all_emotion_ind) == e),emotions);

emotions = full_dataset(emotion_ind,2);
Emotions_Used = arrayfun(@(e) Emotion_Names(e),unique(emotions)')
subjects = full_dataset(emotion_ind,1);
emotion_features = full_dataset(emotion_ind,emotion_features_start+feature_ranks-1);
all_subjects = unique(subjects);
all_emotions = unique(emotions);

if reset_train_test
%     train_all_sub_ind = [1 0 0 0 1 0 1 1 0 0 0 1 1 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 0 1 1 1 1 0 1 1 1 0 0 1];
%     train_all_sub_ind = train_all_sub_ind==1;
%     test_all_sub_ind = ~train_all_sub_ind;
    [train_all_sub_ind,test_all_sub_ind] = crossvalind('HoldOut', length(all_subjects), test_split_percent);
    train_sub_ind = arrayfun(@(e) any(all_subjects(train_all_sub_ind) == e),subjects);
    test_sub_ind = arrayfun(@(e) any(all_subjects(test_all_sub_ind) == e),subjects);
end;

% raw features
trainX = emotion_features(train_sub_ind,:);
trainY = emotions(train_sub_ind);
testX = emotion_features(test_sub_ind,:);
testY = emotions(test_sub_ind);

% z-scores
[ztrainX,muTrainX,sigmaTrainX] = zscore(trainX);
[ztestX,muTestX,sigmaTestX] = zscore(testX);
[ztrainY,muTrainY,sigmaTrainY] = zscore(trainY);
[ztestY,muTestY,sigmaTestY] = zscore(testY);


%% machine setup
machine_sets = {};
set_row = @(name,b,s) {name,b{1},b{2},s};


%% run regular
[mset,best] = emotion_analysis_run_machines(trainX,trainY,testX,testY);
machine_sets{end+1} = set_row('reg',best,mset);

%% run z-score
% [mset,best] = emotion_analysis_run_machines(ztrainX,trainY,ztestX,testY);
% machine_sets{end+1} = set_row('zscore',best,mset);


%% show results
for i=1:length(machine_sets)
    fprintf('%s\t%s %0.6f\n',machine_sets{i}{1},machine_sets{i}{2},machine_sets{i}{3});
    mset = machine_sets{i}{4};
    for j=1:length(mset)
        fprintf('  %s %0.6f\n',mset{j,1},mset{j,2});
    end
end;
