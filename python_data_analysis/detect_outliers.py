from numpy import cov, pi, mean, sqrt, exp, dot, sum
from scipy.linalg import pinv, det


def detect_outliers(X, p=0.005):
    # https://d19vezwu8eufl6.cloudfront.net/ml/docs%2Fslides%2FLecture15.pdf
    m, n = X.shape  # n = number of features
    mu = mean(X, axis=0)
    sigma = cov(X, rowvar=0, ddof=0)
    sigma_inv = pinv(sigma)
    constant_term = 1.0 / ((2.0 * pi) ** (n / 2)) / sqrt(det(sigma))

    def prob(index):
        temp = X[index, :] - mu
        return constant_term * exp(-0.5 * sum(temp * dot(sigma_inv, temp)))

    valid = [index for index in range(m) if prob(index) > p]

    return valid
