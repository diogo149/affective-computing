function [ FX ] = emotion_data_add_time_track( X )
    rows = size(X,1);
    time_track = 1:rows;
    FX = [X time_track'];
end

