from load_data import load_data
from save_data import save_data
from clean_columns import clean_columns
from subprocess import call
from os.path import exists


def make_folders(transform_name):
    directory = "../" + transform_name
    if not exists(directory):
        call(["mkdir", directory])
    directory += "/figures"
    if not exists(directory):
        call(["mkdir", directory])


def transform_data(folder_name, game, subject, transform, transform_name):
    make_folders(transform_name)
    X = load_data(folder_name, game, subject, y=False)
    X = clean_columns(X)
    X = transform(X)
    save_data(X, folder_name, game, subject, category=transform_name, saveplot=True)


def pca_transform(folder_name, game, subject):
    from sklearn.decomposition import PCA
    # from sklearn.decomposition import RandomizedPCA as PCA
    pca = PCA(n_components=2)
    transform_data(folder_name, game, subject, pca.fit_transform, "pca")


def transform_all(transform, transform_name):
    for folder in ['2s', '0.5s']:
        for game in ['escape', 'sahara']:
            for subject in range(1, 44):
                try:
                    transform_data(folder, game, subject, transform, transform_name)
                except IOError:
                    print "failure at %s %s %d %s" % (folder, game, subject, transform_name)
                except AssertionError:
                    print "AssertionError at %s %s %d %s" % (folder, game, subject, transform_name)


def pca_all():
    from sklearn.decomposition import PCA
    pca = PCA(n_components=2)
    transform_all(pca.fit_transform, "pca")

if __name__ == "__main__":
    if 0:
        pca_transform("0.5s", "escape", 1)
    if 0:
        pca_all()
