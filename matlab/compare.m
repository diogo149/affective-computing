%%
Escape_emotions={'Escape-frustrated','Escape-curiosity','Escape-bored','Escape-anxiety','Escape-interest','Escape-challenged','Escape-suspense','Escape-happy','Escape-hope','Escape-anger','Escape-confused','Escape-excited','Escape-enjoy','Escape-dispointed','Escape-surprise','Escape-relaxed','Escape-intrigued','Escape-anoyed','Escape-relieved','Escape-determined','Escape-irritated','Escape-neutral','Escape-regret','Escape-interested','Escape-amused','Escape-hesitate','Escape-mad'};
Sahara_emotions={'Sahara-frustrated','Sahara-curiosity','Sahara-bored','Sahara-anxiety','Sahara-interest','Sahara-challenged','Sahara-suspense','Sahara-happy','Sahara-hope','Sahara-anger','Sahara-confused','Sahara-excited','Sahara-enjoy','Sahara-dispointed','Sahara-surprise','Sahara-relaxed','Sahara-intrigued','Sahara-anoyed','Sahara-relieved','Sahara-determined','Sahara-irritated','Sahara-neutral','Sahara-regret','Sahara-interested','Sahara-amused','Sahara-hesitate','Sahara-mad'};
colorHash=containers.Map;
choiceofcolors=[1 0 0; 0 0 1; 0 1 0; 0 1 1; 0 0 0; 1 0 1; 1 1 0; 0.5, 0.5, 0.5; 0 0 0.5; 0 0.5 0; 0 0.5 0.5; 0.5 0 0; 0.5 0 0.5; 0.5 0.5 0;];


for l=1:length(choiceofcolors)
    colorHash(char(Escape_emotions(l)))=choiceofcolors(l,:);
end

for l=1:length(choiceofcolors)
    colorHash(char(Sahara_emotions(l)))=choiceofcolors(l,:);
end
%same person across games
for iter=1:41
    % Load Escape data
    load(strcat('2sdimreduced/X',num2str(iter)));
    load(strcat('2sdimreduced/label',num2str(iter)));
    escape_X=X;
    escape_label=strcat('Escape-',label)%needed this coz struct was saved
    numescape=size(X,1);
    clear X;
    clear label;

    % Load Sahara data
    load(strcat('2s_sahara/X',num2str(iter)));
    load(strcat('2s_sahara/label',num2str(iter)));
    sahara_X=X;
    sahara_label=strcat('Sahara-',label);
    numsahara=size(X,1);
    clear X;
    clear label;

    %find SNE data
    X=[escape_X;sahara_X];
    label=[escape_label;sahara_label]
    ydata = tsne(X(:,1:60));
    distlabel=unique(label);

    %Draw image
    figure('Visible','off');
    hold on;
    %to change color doing a for loop for scatter plot
    for i=1:numescape
        hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorHash(char(label(i))),'MarkerFaceColor', colorHash(char(label(i))));
    end
    
    for i=numescape+1:numescape+numsahara
        hd(find(strcmp(distlabel,label(i))))=scatter(ydata(i,1),ydata(i,2),20,colorHash(char(label(i))),'^','MarkerFaceColor', colorHash(char(label(i))));
    end
    axis off;
    legend(hd,distlabel,'Location','BestOutside');
    hold off;
    saveas(gcf,strcat('2s_acrossgames/subject',num2str(iter),'.png'));
    clear hd;
    close();  

end
%%