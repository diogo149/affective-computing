from transform_data import transform_all


def kpca_all(kernel="linear"):
    from sklearn.decomposition import KernelPCA
    kpca = KernelPCA(n_components=2, kernel=kernel, gamma=1.0 / 90, eigen_solver='dense', coef0=0)
    transform_name = "kpca_" + kernel
    transform_all(kpca.fit_transform, transform_name)

if __name__ == "__main__":
    # kernels are linear, poly, rbf. sigmoid, cosine
    kpca_all("linear")
