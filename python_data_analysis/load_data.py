from pandas.io.parsers import read_csv


def load_data(folder_name, game, subject, category="data", x=True, y=True):
    assert folder_name in ['2s', '0.5s']
    assert game in ['escape', 'sahara']
    assert subject in range(1, 44)
    filename = "../%s/%s_%s_sub_%02d_" % (category, folder_name, game, subject)
    if x:
        X = read_csv(filename + "X.csv", header=None).as_matrix()
    if y:
        Y = read_csv(filename + "Y.csv", header=None).as_matrix()

    if x and y:
        return X, Y
    if x:
        return X
    if y:
        return Y

if __name__ == "__main__":
    X, Y = load_data('2s', 'escape', 1)
    print X.shape
    print Y.shape
