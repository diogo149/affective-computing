from transform_data import transform_all
from mvpa2.suite import SimpleSOMMapper


def som_fit_transform(X):
    # som = SimpleSOMMapper((20, 30), 400, learning_rate=0.05)
    som = SimpleSOMMapper((10, 10), 100, learning_rate=0.05)
    som.train(X)
    return som(X)


def som_all():
    transform_all(som_fit_transform, "som")

if __name__ == "__main__":
    som_all()
