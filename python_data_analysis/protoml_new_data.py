from protoml.extras.tsne import BarnesHutSNE
from scipy.io import loadmat
import pandas as pd
import numpy as np

perplexity = 30
games = ["escape", "sahara"]
windows = [2, 1]
window_features = {}
window_features[1] = [278, 318, 130, 126, 10, 259, 336, 246, 223, 303, 158, 332, 233, 201, 291, 162, 234, 347, 117, 227, 274, 304, 219, 263, 118, 293, 235, 346, 120, 189, 97, 292, 276, 172, 294, 288, 177, 147, 321, 236, 249, 334, 173, 187, 289, 160, 96, 178, 323, 119, 260, 99, 220, 253, 184, 202, 175, 324, 281, 98, 248, 230, 252, 176, 245, 317, 205, 74, 285, 231, 77, 218, 250, 143, 188, 68, 306, 232, 216, 310, 217, 16, 194, 14, 210, 78, 311, 17, 1, 225, 290, 85, 2, 133, 46, 181, 307, 31, 308, 207]
window_features[2] = [303, 318, 158, 126, 10, 223, 278, 259, 336, 219, 77, 97, 289, 130, 201, 304, 249, 233, 162, 332, 189, 248, 274, 227, 347, 74, 96, 117, 246, 294, 118, 234, 187, 291, 99, 16, 78, 346, 120, 288, 252, 220, 292, 263, 147, 232, 293, 236, 172, 321, 177, 276, 253, 160, 334, 184, 235, 98, 310, 119, 224, 178, 173, 230, 250, 17, 323, 199, 216, 133, 260, 143, 202, 19, 231, 306, 75, 175, 176, 217, 188, 311, 205, 183, 324, 307, 218, 221, 251, 68, 84, 194, 210, 225, 20, 317, 182, 181, 132, 245]


def get_filename(game, window):
    assert game in games
    assert window in windows
    return "../new_data/%s_all_window%d.mat" % (game, window)


def read_mat(game, window):
    """because of indexing from 0 and skipping the first 2 columns, we need to add 1 to each of the feature numbers
    """
    filename = get_filename(game, window)
    features = [0, 1] + map(lambda x: x + 1, window_features[window])
    return pd.DataFrame(loadmat(filename)["new"])[features]


def split_by(df, column):
    """generates data frames for each unique value in specified column.
    """
    tmp = df[column]
    for val in tmp.unique():
        yield df[tmp == val]


def preprocess(df):
    """scales data frame to 0 mean and 1 std, fills missing values, then returns as a numpy matrix
    """
    return ((df - df.mean()) / df.std()).fillna(0).as_matrix()


if __name__ == "__main__":
    clf = BarnesHutSNE(perplexity=perplexity)
    for game in games:
        for window in windows:
            final = pd.DataFrame()
            data = read_mat(game, window)
            for df in split_by(data, 0):
                subject, emotion = df[0], df[1]
                del df[0], df[1]
                transformed = clf.fit_transform(preprocess(df))
                transformed = pd.DataFrame(transformed)
                transformed["subject"] = np.array(subject)
                transformed["emotion"] = np.array(emotion)
                final = final.append(transformed)
            final.to_csv(get_filename(game, window) + ".csv")
